<%@page language="java"
        contentType="text/html; charset=ISO-8859-1"
	    import="net.fseconomy.data.*, net.fseconomy.util.Formatters"
%>

<jsp:useBean id="user" class="net.fseconomy.beans.UserBean" scope="session" />

<%
	boolean isTestServer;
	if(System.getProperty("isTestServer") == null)
	{
		isTestServer = request.getRequestURL().toString().contains("8080");
		System.setProperty("isTestServer", isTestServer ? "true" : "false");
	}
	else
		isTestServer = 	"true".equals(System.getProperty("isTestServer"));

%>
<div style="background-color: lightcoral; color: #ffffff; display: <%=isTestServer ? "block" : "none"%>">
	Test Server!!
</div>
<div class="header <%= user.isLoggedIn() ? "loggedin" : "" %>">
	<div class="header-xs">
		<div class="navicon"><i class="fa fa-bars"></i></div>
		<h1>FSEconomy</h1>
	</div>
	<div id="login-block">
	<%
		if (!user.isLoggedIn())
		{
	%>
		<script type="text/javascript">
			function submit()
			{
				document.loginform.action="<%= response.encodeURL("/requestnewpassword.jsp") %>";
				document.loginform.submit();
			}
		</script>
		
		<form name="loginform" method="post" action="userctl">
			<input type="hidden" name="offset" id="offset" value=""/>
			
			<script type="text/javascript">
				document.getElementById("offset").value = (new Date()).getTimezoneOffset()/60 * (-1);
			</script>
			<ul>
				<li>
					<label for="name">Username</label>
					<input type="text" maxlength="64" name="user" id="name" />	
				</li>
				<li>
					<label for="password">Password</label>
					<input type="password" name="password" id="password" />
				</li>
				<li class="form-actions">
					<input type="submit" name="event" value="Agree & Log in" />
					<input type="button" onClick="location.href='requestnewpassword.jsp';" class="forgot-password" value="Forgot Password" />
				</li>
			</ul>
			<p>I will follow the <a href="http://fseconomy.net/tos" target="_blank">Rules of Fair Play</a></p>
		</form>
	<%
		} 
		else 
		{
			Banking.reloadMoney(user);
			int hours = 48;
			double totalhours;
			String stotalhours;

			totalhours = Stats.getInstance().getNumberOfHours(user.getId(), hours);
			stotalhours = Formatters.twoDecimals.format(totalhours);
	%>
		<form class="top user-data" method="post" action="/userctl">
			<p>Logged in as <jsp:getProperty name="user" property="name"/> <input type="submit" name="event" class="button" value="Log out" /></p>
			<ul>
				<li>
					<strong>Cash balance:</strong>
					<%= Formatters.currency.format(user.getMoney()) %>
				</li>
				<li>
					<strong>Bank balance:</strong>
					<%= Formatters.currency.format(user.getBank())%>
				</li>
				<li>
	<% 		
			if(totalhours > 30)
			{
	%>
				<span class="warning"><strong>HOURS EXCEEDED!</strong></span> <a class="normal" href="<%= response.encodeURL("hours.jsp") %>"><%= stotalhours %></a> in last 48
	<% 		
			} 
			else 
			{
	%>
				<strong>Hours Flown:</strong> <a class="normal" href="<%= response.encodeURL("hours.jsp") %>"><%= stotalhours %></a> <span>in last 48</span>
	<%
			}
	%>
				</li>
			</ul>
		</form>
	<%	
		}
	%>
	</div>
</div>

