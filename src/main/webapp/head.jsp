<!doctype html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FSEconomy</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
	<link href="/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon">
    <link href="/css/redmond/jquery-ui.css" rel="stylesheet" type="text/css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel='stylesheet prefetch' type="text/css">
    <link href="/css/Master.css?20200206" rel="stylesheet" type="text/css">
    <link href="/css/tablesorter-style.css" rel="stylesheet" type="text/css">
    
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script type='text/javascript' src="/scripts/jquery.cookie.js"></script>
    <script type='text/javascript' src='/scripts/jquery.tablesorter.js'></script>
    <script type='text/javascript' src="/scripts/jquery.tablesorter.widgets.js"></script>
    <script type='text/javascript' src='/scripts/parser-checkbox.js'></script>
    <script type='text/javascript' src='/scripts/parser-timeExpire.js'></script>
    <script type='text/javascript' src='../scripts/parser-timeHrMin.js'></script>
    <script src="/scripts/AutoComplete.js"></script>
    <script src="/scripts/js/highcharts.js"></script>
    <script src="/scripts/PopupWindow.js"></script>
    <script src="/scripts/doubletaptogo.js"></script>	

    <script>
        $( document ).ready(function() {
            $(".navicon").on("click", function() {
                $( "body" ).toggleClass("menu-visible");
                $(".navicon i").toggleClass("fa-times").toggleClass("fa-bars");
            }); 
        });
    </script>
</head>