<%@page language="java"
        contentType="text/html; charset=ISO-8859-1"
        import=" java.util.List, net.fseconomy.beans.*,  net.fseconomy.data.*"
%>
<%@ page import="net.fseconomy.util.Converters" %>
<%@ page import="net.fseconomy.util.Formatters" %>
<%@ page import="net.fseconomy.dto.FboLottery" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Date" %>

<jsp:useBean id="user" class="net.fseconomy.beans.UserBean" scope="session" />

<%
    if(user == null || !user.isLoggedIn())
    {
%>
<script type="text/javascript">document.location.href="/index.jsp"</script>
<%
        return;
    }

    String error = null;
    String sBuyId = request.getParameter("buy");

    if(sBuyId != null && !sBuyId.isEmpty())
    {
        int id = Integer.parseInt(sBuyId);
        try
        {
            Fbos.buyFboLotteryTicket(user.getId(), id);

        }
        catch(DataError e)
        {
            error = e.getMessage();
        }
    }
    List<FboBean> fbos;

    String title = "FBOs open for lottery ticket purchases";
    fbos = Fbos.getFboLotteryActive();

    String lottoDate;
    if(fbos.size() > 0)
    {
        Date lotteryDate = new Date();
        lotteryDate = Fbos.getFboLottery(fbos.get(0).getId()).end;
        lotteryDate.setMinutes(0);
        lottoDate = Formatters.dateyyyymmddhhmmzzz.format(lotteryDate);
    }
    else
        lottoDate = "No current lottery";
%>
<jsp:include flush="true" page="/head.jsp" />
    <script type="text/javascript">
        var gmapfs = new PopupWindow();
        var gmap = new PopupWindow();

        $(function()
        {
            $.extend($.tablesorter.defaults, {
                widthFixed: false,
                widgets : ['zebra','columns']
            });

            $('.fboTable').tablesorter();

            $(".clickableRow").click(function() {
                window.document.location = $(this).data("url");
            });

            $("#newTemplateButton").click(function() {
                window.document.location = "/admin/templateedit.jsp?newtemplate=1";
            });
        });

        function doTicketPurchase(fboid, icao)
        {
            if (window.confirm("Are you sure want to buy a ticket for " + icao + "?\nYou cannot change your selection once purchased!"))
            {
                document.location.href = "fbolotteries.jsp?buy=" + fboid;
            }
        }
    </script>
</head>
<body>

<jsp:include flush="true" page="top.jsp" />
<jsp:include flush="true" page="menu.jsp" />

<div id="wrapper">
    <div class="content">
<%
    if (error != null)
    {
%>
        <div class="error"><%= error %></div>
<%
    }
%>
        <p style="font-size: large;">
            <h3>Welcome to the FBO Lottery Page</h3>
            Winners will be selected on the date indicated and notified by email.<br>
            <span style="color: red;">Each ticket purchase is final!</span><br><br>
            FBOs that have no tickets purchased during the active lottery period will be marked for tear down sometime in the next 7 days.
        </p>
        <p>
            <h1>Drawing will be held:  <%= lottoDate %></h1>
        </p>
        <table class="fboTable tablesorter-default tablesorter" style="width: auto;">
        <caption>
            <%=title%> (<%=fbos.size()%>) <a href="#" onclick="gmapfs.setSize(690,535);gmapfs.setUrl('<%= response.encodeURL("gmapfboLottery.jsp") %>');gmapfs.showPopup('gmapfs');return false;" id="gmapfs"><img src="img/wmap.gif" width="50" height="32" border="0" align="absmiddle" /></a>
        </caption>
        <thead>
        <tr>
            <th class="normal" style="width: 70px;">Action</th>
            <th class="normal" style="width: 85px;">Any Purchased?</th>
            <th class="normal" style="width: 85px;">ICAO</th>
            <th class="numeric" style="width: 45px;">Lots</th>
            <th class="numeric" style="width: 55px;">Upgrades</th>
            <th class="numeric" style="width: 85px;">Ticket Amount</th>
        </tr>
        </thead>
        <tbody>
<%
    for (FboBean fbo : fbos)
    {
        CachedAirportBean ap = Airports.cachedAirports.get(fbo.getLocation());
        FboLottery fl = Fbos.getFboLottery(fbo.getId());
        boolean hasTicket = Fbos.hasFboLotteryTicket(user.getId(), fl.id);

        String upgrades = "None";
        if(fbo.hasPassengerTerminal() && fbo.hasRepairshop())
            upgrades = "PT/MF";
        else if(fbo.hasRepairshop())
            upgrades = "MF";
        else if(fbo.hasPassengerTerminal())
            upgrades = "PT";
%>
            <tr>
                <td class="normal"><%=hasTicket ? "<span style=\"color: green;\">Purchased</span>" : "<a href=\"#\" onclick='doTicketPurchase(" + fbo.getId() + ", \"" + fbo.getLocation() + "\")'>Purchase</a>" %></td>
                <td class="normal"><%= fl.ticketsSold > 0 ? "Yes" : "No" %></td>
                <td class="nowrap"><%= Airports.airportLink(ap.getIcao(), ap.getIcao(), response) %></td>
                <td class="numeric"><%= fbo.getFboSize() %></td>
                <td class="normal"><%= upgrades %></td>
                <td class="numeric"><%= Formatters.currency.format( fl.ticketAmount) %></td>
            </tr>
<%
    }
%>
            </tbody>
        </table>
        <p>.</p>
    </div>
</div>
</body>
</html>

