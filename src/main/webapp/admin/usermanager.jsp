<%@page language="java"
        contentType="text/html; charset=ISO-8859-1"
        import="net.fseconomy.data.*, net.fseconomy.util.Helpers, net.fseconomy.beans.UserBean, java.util.*"
%>

<jsp:useBean id="user" class="net.fseconomy.beans.UserBean" scope="session" />
<%@ page import="net.fseconomy.dto.ClientRequest" %>

<%
    if(!Accounts.needLevel(user, UserBean.LEV_CSR) && !Accounts.needLevel(user, UserBean.LEV_MODERATOR))
    {
%>
<script type="text/javascript">document.location.href="../index.jsp"</script>
<%
        return;
    }

    String returnPage = request.getRequestURI();
    String message = Helpers.getSessionMessage(request);
    if(message == null)
        message = "";
%>
<jsp:include flush="true" page="/head.jsp" />
    <script src="../scripts/AdminAutoComplete.js"></script>
    <script src="../scripts/AutoCompleteEmail.js"></script>
    <script src="../scripts/AutoCompleteIP.js"></script>

    <script type="text/javascript">

        $(function()
        {
            initAutoComplete("#username", "#userid", <%= Accounts.ACCT_TYPE_PERSON %>);
            initAutoCompleteEmail("#email", "#emailuserid", <%= Accounts.ACCT_TYPE_PERSON %>);
            initAutoCompleteIP("#ip", "#searchby");

        });

    </script>

    <script type="text/javascript">

        function doViewAccount(name)
        {
            var form = document.getElementById(name);

            if(name == 'SearchByEmail')
                form.userid.value = form.emailuserid.value;

            if(name == 'SearchByIP')
                form.userid.value = form.ipuserid.value;

            form.submit();
        }

    </script>
</head>
<body>

<jsp:include flush="true" page="/top.jsp" />
<jsp:include flush="true" page="/menu.jsp" />

<div id="wrapper">
    <div class="content">
        <a href="/admin/admin.jsp">Return to Admin Page</a><br/>

        <p><%= message %></p>

        <div class="form" style="width: 500px">
            <h2>Sign up</h2>
            <p>To sign up, enter your email address and a user name in the form below.</p>

            <form method="post" action="/userctl">
                <div>
                    <input type="hidden" name="event" value="create">
                    <input type="hidden" name="returnpage" value="<%=returnPage%>">
                </div>
                Username<br>
                <input name="user" type="text" class="textarea" size="50" maxlength="45"><br>
                Email<br/>
                <input name="email" type="text" class="textarea" size="50"  maxlength="45"><br><br>
                <input type="submit" class="button" value="Sign up">
            </form>
        </div>

        <div class="form" style="width: 400px">
            <h2>Search by User Account</h2>
            <p>
            </p>

            <form id="SearchByName" method="post" action="usermanageredit.jsp">
                <div class="formgroup">
                    Enter Account:
                    <input type="hidden" id="userid" name="userid" value=""/>
                    <input type="text" id="username" name="username"/>
                    <br/>
                </div>

                <div class="formgroup">
                    <input type="button" class="button" onclick="doViewAccount('SearchByName')" value="View Account" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="hidden" name="returnpage" value="<%= returnPage %>"/>
                </div>
            </form>
        </div>

        <div class="form" style="width: 400px">
            <h2>Search by Email</h2>
            <p>
            </p>

            <form id="SearchByEmail" method="post" action="usermanageredit.jsp">
                <div class="formgroup">
                    Enter Account:
                    <input type="hidden" id="emailuserid" name="emailuserid" value=""/>
                    <input type="hidden" id="userid" name="userid" value=""/>
                    <input type="text" id="email" name="email"/>
                    <br/>
                </div>

                <div class="formgroup">
                    <input type="button" class="button" onclick="doViewAccount('SearchByEmail')" value="View Account" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="hidden" name="returnpage" value="<%= returnPage %>"/>
                </div>
            </form>
        </div>

<%
	String searchby = request.getParameter("searchby");
%>
        <h2>Client IP Checker</h2>
        <h4>Enter partial or full IP (min 3 numbers) and wait for popup, click entry to select desired user</h4>
        <h4>Select Ip</h4>
        <div class="form" style="width: 400px">
            <form method="post">
                <div>
                    IP:
                    <input type="text" id="ip" name="ip"/>
                    <br/>
                    <input type="submit" class="button" value="GO" />
                    <input type="hidden" id="searchby" name="searchby" value=""/>
                </div>
            </form>
        </div>
<%
	if (searchby != null && !searchby.equals(""))
	{
		List<ClientRequest> list;

        list = SimClientRequests.getClientRequestsByIp(searchby);

		if (message != null)
		{
%>
        <div class="message"><%= message %></div>
<%
		}

		if (list.size() > 0)
		{
%>	<div class="dataTable">
        <h2>IP - <%= searchby %></h2><br/>
        <table id="sortableTableStats" class="sortable">
            <thead>
            <tr>
                <th>name</th>
                <th>hits</th>
            </tr>
            </thead>

            <tbody>
<%
            for (ClientRequest item: list)
            {
%>
            <tr>
                <td>
                    <%= item.name %>
                </td>
                <td>
                    <%= item.ip %>
                </td>
            </tr>
<%
            }
%>
            </tbody>
        </table>
    </div>
<%
        }
    }
%>
    </div>
</div>
</body>
</html>
