<%@page language="java"
        contentType="text/html; charset=ISO-8859-1"
        import="net.fseconomy.data.*, net.fseconomy.beans.*, java.util.*"
%>
<%@ page import="static net.fseconomy.data.SimClientRequests.*" %>
<%@ page import="static net.fseconomy.data.Airports.cachedAirports" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="net.fseconomy.dto.AirportInfo" %>

<jsp:useBean id="user" class="net.fseconomy.beans.UserBean" scope="session" />

<%
    if (!user.isLoggedIn())
    {
%>
<script type="text/javascript">document.location.href = "/index.jsp"</script>
<%
        return;
    }
%>

<%
    response.setContentType("text/html");

    String search = request.getParameter("startsWith");
    if(search == null)
        search = "k";

    try
    {
//        Map<Integer, String> hmap = new HashMap<Integer, String>();
//        hmap.put(11, "Apple");
//        hmap.put(22, "Orange");
//        hmap.put(33, "Kiwi");
//        hmap.put(44, "Banana");
//
//        Map<Integer, String> result = hmap.entrySet()
//                .stream()
//                .filter(map -> "Orange".equals(map.getValue()))
//                .collect(Collectors.toMap(map -> map.getKey(), map -> map.getValue()));

        List<String> icaos = Airports.searchForIcao(search);

        if(icaos.size() > 0)
        {
            String output = "{\"icaos\": [";
            boolean firstLoop = true;
            for(String icao : icaos)
            {
                if(!firstLoop)
                    output += ",";
                else
                    firstLoop = false;

                output += "{\"icao\":\"" + icao + "\"}\n";
            }
            output += "]}";

%>
<%= output %>
<%
    }
}
catch(Exception e)
{
%>
"Exception is: <%= e %>"
<%
    }
%>
