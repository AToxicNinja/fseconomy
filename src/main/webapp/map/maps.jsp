<%@page language="java"
        contentType="text/html; charset=ISO-8859-1"
        import="java.util.List, net.fseconomy.beans.*, net.fseconomy.data.*, net.fseconomy.util.*"
%>

<jsp:useBean id="user" class="net.fseconomy.beans.UserBean" scope="session"/>

<%
    String message = "";
    if (!user.isLoggedIn())
    {
%>
<script type="text/javascript">document.location.href = "/index.jsp"</script>
<%
            message = "You must login to use the Map page";
        }
%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>FSE Maps</title>

    <link prefetch href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <style>
    </style>

    <jsp:include flush="true" page="gmapapikey.jsp"></jsp:include>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="scripts/map-utils.js"></script>
    <script src="scripts/airport-utils.js"></script>

    <script>
        window.onbeforeunload = function (e) {
            message = "Are you sure you want leave?";
            e.returnValue = message;
            return message;
        };
    </script>

    <script type="text/javascript">
        function initMap() {
//-----------------------------------------------------------------------------------------------------
// Map functions
            //add in missing functions, must be after Google Maps is loaded but before any other calls
            addMapPrototypes();

            function projection_changed() {
                map.setZoom(google.maps.prototype.getZoomByBounds(map, bounds) - 1);
            }

//-----------------------------------------------------------------------------------------------------

            // The map, centered at Uluru
            var map = new google.maps.Map(document.getElementById('map'),
                    {
                        zoom: 12,
                        //center: {lat: clat, lng: clng},
                        mapTypeId: google.maps.MapTypeId.SATELLITE,
                        mapTypeControl: true,
                        zoomControl: true,
                        scaleControl: false,
                        streetViewControl: false,
                        fullscreenControl: false
                    });

            google.maps.event.addListenerOnce(map, 'projection_changed', projection_changed);
//----------------------------------------------------------------------------------------------------
            //setup default infoWindow
            infoWindowGlobal = new google.maps.InfoWindow({
                content: "No content set"
            });

            createMapMarker(map, MarkerTypeEnum.Depart, pointloc);

            var content = "";

            google.maps.event.addListener(infoWindow, 'domready', function() {
                $("#tabs").tabs();
            });

            marker.addListener('click', function () {
                var tabData = new tabContent('Airport', 'Airport data here');
                var out = c.join('');
                infoWindow.setContent(out);
                infoWindow.open(map, marker);
            });

            var markerDest;
            if (<%=isDest%>) {
                //SET DISTANCE & BEARING
                var degrees = Math.round(google.maps.bearing(pointloc, pointdest));
                var msg = "Trip: " + '<font color="#0080C0"><%=icao%></font>' + " to " + '<font color="#0080C0"><%=icaod%></font>' + "&nbsp;&nbsp;&nbsp;&nbsp;Distance: " + "<span style=\"color: #0080C0; \">" + Math.round(pointloc.distanceFrom(pointdest)) + " NM </font>&nbsp;&nbsp;&nbsp;&nbsp;Bearing: " + "<span style=\"color: #0080C0; \">" + degrees + "&#186;</font>";
                document.getElementById("mypoint").innerHTML = msg;


                markerDest = createMapMarker(map, MarkerTypeEnum.Dest, pointdest);

                var apContent = "<font face='Verdana' size='1'><%=airportd.getIcao()%><br><%=airportd.getTitle()%></font>";
                var jobContent = "";
                var fboContent = "";

                var tabs = createTabName("Airport", 1);
                var tabsContent = createTabContent(apContent, 1);

                <%
                        assignments = Assignments.getAssignments(icaod, -1, -1, -1, -1);
                        CachedAirportBean destination;

                        if (assignments.size() != 0)
                        {
                %>
                jobContent += "<table>";
                //start header
                jobContent += "<tr>";
                jobContent += "<td>Dir</td>";
                jobContent += "<td>Dist</td>";
                jobContent += "<td>ICAO</td>";
                jobContent += "<td>Pay</td>";
                jobContent += "<td>Name</td>";
                jobContent += "<td>Type</td>";
                jobContent += "<td>Expires</td>";
                jobContent += "</tr>";
                //end header
                <%
                                for (AssignmentBean bean : assignments)
                                {
                                    assignment = bean;
                                    destination = assignment.getDestinationAirport();
                                    image = "<img src='img/set2_"+ assignment.getBearingImage() + ".gif'>";
                                    type = assignment.getType() == AssignmentBean.TYPE_ALLIN ? "A" : "T";
                                    String row =
                                        "<td>" + image + "</td>" +
                                        "<td>" + assignment.getDistance() + " nm</td>" +
                                        "<td>" + destination.getIcao() + "</td>" +
                                        "<td>" + assignment.calcPay() + "</td>" +
                                        "<td>" + Converters.escapeJavaScript(assignment.getSCargo()) + "</td>" +
                                        "<td>" + type  + "</td>" +
                                        "<td>" + assignment.getSExpires() + "</td>";
                %>
                jobContent += "<tr>";
                jobContent += "<%=row%>";
                jobContent += "</tr>";
                <%
                                }
                %>
                //start content

                //end content
                jobContent += "</table>";

                tabs += createTabName("Jobs", 2);
                tabsContent += createTabContent(jobContent, 2);
                <%
                        }

                        fboList = Fbos.getFboByLocation(icaod);
                        fuelprice = airportd.getPrice100ll();
                        String fuel = Formatters.currency.format(fuelprice);
                        isFBO = false;

                        if(fboList.size() != 0 || airportd.has100ll())
                        {
                %>
                fboContent += "<table>";
                //start header
                fboContent += "<tr>";
                fboContent += "<td>Name</td>";
                fboContent += "<td>100LL Price</td>";
                fboContent += "<td>Gals Avail</td>";
                fboContent += "<td>Repair Shop</td>";
                fboContent += "</tr>";
                //end header
                <%
                            isFBO = true;
                            String row;

                            for (FboBean fbo : fboList)
                            {
                                String temp  ;
                                fuelleft = Goods.getGoods(fbo.getLocation(), fbo.getOwner(), GoodsBean.GOODS_FUEL100LL);
                                int fuelgallons = 0;
                                if (fuelleft != null)
                                    fuelgallons = (int)Math.floor(fuelleft.getAmount()/Constants.GALLONS_TO_KG);

                //                temp = Converters.escapeJavaScript(fbo.getName())+" |  Fuel: " + Formatters.currency.format(fbo.getFuel100LL())+" | "+ fuelgallons + " gals";
                                String repairs = (fbo.getServices() & FboBean.FBO_REPAIRSHOP) > 0 ? "Yes" : "No";

                                row =
                                    "<td>" + Converters.escapeJavaScript(fbo.getName()) + "</td>" +
                                    "<td>" + Formatters.currency.format(fbo.getFuel100LL()) + "</td>" +
                                    "<td>" + fuelgallons + "</td>" +
                                    "<td>" + repairs + "</td>";
                %>
                fboContent += "<tr>";
                fboContent += "<%=row%>";
                fboContent += "</tr>";
                <%
                            }

                            if (airportd.has100ll())
                            {
                                String repairs = airportd.getSize() >= AircraftMaintenanceBean.REPAIR_AVAILABLE_AIRPORT_SIZE ? "Yes" : "No";
                                row =
                                    "<td>Local</td>" +
                                    "<td>" + fuel + "</td>" +
                                    "<td>Unlimited</td>" +
                                    "<td>" + repairs + "</td>";
                %>
                fboContent += "<tr>";
                fboContent += "<%=row%>";
                fboContent += "</tr>";
                <%
                            }

                %>
                //start content

                //end content
                fboContent += "</table>";

                tabs += createTabName("FBOs", 3);
                tabsContent += createTabContent(fboContent, 3)
                <%
                        }
                %>
            }

            markerDest.addListener('click', function () {
                var tabT = contentTemplate.slice(0); //deep copy for strings
                tabT[2] = tabs;
                tabT[4] = tabsContent;
                var out = tabT.join('');
                infoWindow.setContent(out);
                infoWindow.open(map, marker);
            });
        }
    </script>
</head>
<body onload="initMap()" class="biggie" >

<div class="container">
    <div class="row col-xs-12">
        <h1>
            FSE Maps - Global Map
        </h1>

        <p>
            <div>
                Map display title here - dynamically changes based on requested map
            </div>

            <div id="map">
                Map here
            </div>
        </p>

        <p>
            Please consider leaving this window/tab open till you are finished with FSE for the day to reduce our quota hits on Google Maps. Thank you!
        </p>
    </div>
    <div class="col-xs-12">

        <div class="container">
            <div class="row">
                <div class="container-fluid">
                    <hr />
                    <p>
                        <a href="https://www.facebook.com/FSEconomy" target="_blank"><img src="img/Facebook.png" alt="Facebook" /></a>
                        <a href="https://plus.google.com/+Fseconomy" target="_blank"><img src="img/GooglePlus.png" alt="GooglePlus" /></a>
                        <a href="https://twitter.com/fseconomy" target="_blank"><img src="img/Twitter.png" alt="Twitter" /></a>
                        <a href="http://www.youtube.com/fseconomy" target="_blank"><img src="img/YouTube.png" alt="YouTube" /></a>
                        <a href="mailto:fsesupport@gmail.com?subject=FSEconomy"><img src="img/email.png" alt="email" /></a>
                    </p>
                    <p>&nbsp;<a target="_blank" href="https://www.fseconomy.net/tos">Terms of Service</a></p>
                <p>&copy; FSE Communities 2019</p>
            </div>
        </div>
    </div>
    </div>
</div>

</body>
</html>