package net.fseconomy.data;

import net.fseconomy.beans.*;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Expenses implements Serializable
{

    public final static Double HOTEL_COST = 150.0;
    public final static Double RENTALCAR_COST = 45.0;
    public final static Double MEALS_COST = 35.0;
    public final static Double TIEDOWN_COST = 50.0;
    public final static Double WASH_COST = 125.0;
    public final static Double QT_OIL_COST = 12.50;
    public final static int MAX_QTS_OIL = 24;
    public final static Double O2_REFILL_COST = 25.00;

    public final static Double CUSTOM_COST_MAX = 2500.0;

    final static Double TICKET_COST_PER_NM = 2.0;
    final static Double TICKET_COST_MAX = 2500.0;

    static
    {
    }

    public static double computeExpenseCost(int totalDays, boolean hotel, boolean rentalcar, boolean meals, boolean tiedown, boolean wash, int quartsOil, boolean o2Refill, int custExpCost)
    {
        double totalCost = 0.0;

        totalCost += HOTEL_COST * totalDays * (hotel ? 1 : 0);
        totalCost += RENTALCAR_COST * totalDays * (rentalcar ? 1 : 0);
        totalCost += MEALS_COST * totalDays * (meals ? 1 : 0);
        totalCost += TIEDOWN_COST * totalDays * (tiedown ? 1 : 0);
        totalCost += WASH_COST * (wash ? 1 : 0);
        totalCost += QT_OIL_COST * quartsOil;
        totalCost += O2_REFILL_COST * (o2Refill ? 1 : 0);
        totalCost += custExpCost;

        return totalCost;
    }

    public static double computeTicketCost(String from, String to)
    {
        double dist = Airports.getDistance(from, to);
        double cost =  dist * TICKET_COST_PER_NM;
        if(cost > TICKET_COST_MAX)
        {
            cost = 2500.0;
        }
        return cost;
    }
}
