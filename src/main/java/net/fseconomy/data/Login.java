package net.fseconomy.data;

import net.fseconomy.util.Constants;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * Tracks failed login attempts and provides lockout period monitoring.
 */
public class Login
{
    //set default
    private static int MAX_ATTEMPTS_DEFAULT = 5;
    private static int LOCK_PERIOD_SECONDS_DEFAULT = Constants.SECONDS_PER_MINUTE * 5; // 5 minutes

    //try and get db settings
    private static int MAX_ATTEMPTS = getMaxAttempts();
    private static int LOCK_PERIOD_SECONDS = getLockingPeriod();

    String name;
    int attempts;
    Boolean locked = false;
    Timestamp firstLoginAt;
    Timestamp lockedAt;

    public Login(String userName)
    {
        name = userName;
    }

    private static int getSysVar(String varName) throws SQLException
    {
        String sql = "SELECT value FROM sysvariables WHERE VariableName = ?";
        return DALHelper.getInstance().ExecuteScalar(sql, new DALHelper.IntegerResultTransformer(), varName);
    }

    private static int getMaxAttempts()
    {
        try
        {
            int val = getSysVar("login.maxAttempts");
            return val <= 0 ? MAX_ATTEMPTS_DEFAULT : val;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return MAX_ATTEMPTS_DEFAULT;
        }
    }

    private static int getLockingPeriod()
    {
        try
        {
            int val = getSysVar("login.lockPeriod");
            return val <= 0 ? LOCK_PERIOD_SECONDS_DEFAULT : val;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return LOCK_PERIOD_SECONDS_DEFAULT;
        }
    }

    public void failed()
    {
        attempts++;
        if (attempts == 1)
            firstLoginAt = now();

        if (attempts >= MAX_ATTEMPTS)
        {
            locked = true;
            lockedAt = now();
        }
    }

    public Boolean isLocked()
    {
        return locked && inLockout();
    }

    private boolean inLockout()
    {

        long elapsed = now().getTime() - lockedAt.getTime();
        long period = LOCK_PERIOD_SECONDS * 1000L;

        if(elapsed <= period)
        {
            return true;
        }
        else
        {
            locked = false;
            attempts = 0;
            return false;
        }
    }

    private Timestamp now()
    {
        return Timestamp.valueOf(LocalDateTime.now());
    }
}