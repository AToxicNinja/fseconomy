package net.fseconomy.util;

import java.util.regex.Pattern;

/**
 * Created by smobley on 11/4/2014.
 */
public class Constants
{
    static{}

    public static final int stepSize = 20;

    public static final String systemLocation = "https://server.fseconomy.net";

    public static long MILLISECS_PER_MIN = (60*1000);
    public static long MILLISECS_PER_HOUR = (60*60*1000);
    public static long MILLISECS_PER_DAY = MILLISECS_PER_HOUR * 24;

    public static int SECONDS_PER_MINUTE = 60;

    public static final double GALLONS_TO_KG = 2.68735;

    public static final int PASSENGER_WT_KG = 77;

    public static final String PASSWORD_ALLOWED_CHARACTERS = "0-9a-zA-Z.-_~!@#$^";
    public static final Pattern PASSWORD_PATTERN = Pattern.compile("^[0-9a-zA-Z.\\-_~!@#$^]{12,40}$");
    public static final Pattern USERNAME_PATTERN = Pattern.compile("^([a-zA-Z0-9_.-]){3,40}$");
    public static final Pattern EMAIL_PATTERN = Pattern.compile(
        "^[a-zA-Z0-9_#$%&’*+/=?^.-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$");
    public static final Pattern URL_PATTERN = Pattern.compile(
        "^(http|https)://[-a-zA-Z0-9+&@#/%?=~_|,!:.;]*[-a-zA-Z0-9+@#/%=&_|]");

}
