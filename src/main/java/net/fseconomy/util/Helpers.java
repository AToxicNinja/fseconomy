package net.fseconomy.util;

import net.fseconomy.data.DALHelper;

import javax.servlet.http.HttpServletRequest;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.regex.Pattern;

import net.fseconomy.util.Constants;

public class Helpers
{
    public static String getSessionMessage(HttpServletRequest request)
    {
        String message = (String)request.getSession().getAttribute("message");
        if(message != null)
            request.getSession().setAttribute("message", null);
        else
            return null;

        return message;
    }

    public static String getSessionIcao(HttpServletRequest request)
    {
        String icao = (String)request.getSession().getAttribute("icao");

        return icao;
    }

    public static String getSessionReturnUrl(HttpServletRequest request)
    {
        String url = (String)request.getSession().getAttribute("returnUrl");
        if(url != null)
            request.getSession().setAttribute("back", null);
        else
            return "javascript:window.history.back();";

        return url;
    }

    public static boolean isNullOrBlank(String s)
    {
        return (s==null || s.trim().equals(""));
    }

    //ok performance, if you need this A LOT find something else.
    public static boolean isInteger( String input ) {
        try
        {
            Integer.parseInt( input );
            return true;
        }
        catch( Exception e )
        {
            return false;
        }
    }
    private static final Pattern DOUBLE_PATTERN = Pattern.compile(
            "[\\x00-\\x20]*[+-]?(NaN|Infinity|((((\\p{Digit}+)(\\.)?((\\p{Digit}+)?)" +
                    "([eE][+-]?(\\p{Digit}+))?)|(\\.((\\p{Digit}+))([eE][+-]?(\\p{Digit}+))?)|" +
                    "(((0[xX](\\p{XDigit}+)(\\.)?)|(0[xX](\\p{XDigit}+)?(\\.)(\\p{XDigit}+)))" +
                    "[pP][+-]?(\\p{Digit}+)))[fFdD]?))[\\x00-\\x20]*");

    public static boolean isFloat(String s)
    {
        return DOUBLE_PATTERN.matcher(s).matches();
    }

    public static boolean isDouble(String s)
    {
        if(s == null) return false;
        return DOUBLE_PATTERN.matcher(s).matches();
    }

    public static String truncate(String str, int maxLen)
    {
        if(str == null)
            return str;

        return (str.length() < maxLen) ? str : str.substring(0, maxLen);
    }

    public static String padRight(String s, int n) {
        return String.format("%1$-" + n + "s", s);
    }

    public static String padLeft(String s, int n) {
        return String.format("%1$" + n + "s", s);
    }

    public static boolean isEmailValid(String email)
    {
        if (email == null)
            return false;
        return Constants.EMAIL_PATTERN.matcher(email).matches();
    }

    public static boolean isUsernameValid(String name)
    {
        if (name == null)
            return false;
        return Constants.USERNAME_PATTERN.matcher(name).matches();
    }

    public static boolean isPasswordValid(String password)
    {
        if (password == null)
            return false;
        return Constants.PASSWORD_PATTERN.matcher(password).matches();
    }

    public static boolean verifyURL(String url)
    {
        if(url == null)
            return false;

        String urlDecoded = URLDecoder.decode(url);
        return Constants.URL_PATTERN.matcher(url).matches() && Constants.URL_PATTERN.matcher(urlDecoded).matches();
    }

    public static int getSysVariableInteger(String name, int defaultValue)
    {
        try
        {
            String qry = "SELECT value FROM sysvariables WHERE variablename=?";
            int value = DALHelper.getInstance().ExecuteScalar(qry, new DALHelper.IntegerResultTransformer(), name);
            return value;
        }
        catch(SQLException e)
        {
            e.printStackTrace();
            return defaultValue;
        }
    }

    public static String getSysVariableString(String name, String defaultValue)
    {
        try
        {
            String qry = "SELECT svalue FROM sysvariables WHERE variablename=?";
            String value = DALHelper.getInstance().ExecuteScalar(qry, new DALHelper.StringResultTransformer(), name);
            return value;
        }
        catch(SQLException e)
        {
            e.printStackTrace();
            return defaultValue;
        }
    }
}
