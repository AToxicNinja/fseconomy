package net.fseconomy.servlets;

    import java.io.IOException;
    import java.util.Date;

    import javax.servlet.Filter;
    import javax.servlet.FilterChain;
    import javax.servlet.FilterConfig;
    import javax.servlet.ServletException;
    import javax.servlet.ServletRequest;
    import javax.servlet.ServletResponse;
    import javax.servlet.http.HttpServletRequest;
    import javax.servlet.http.HttpSession;

public class SessionFilter implements Filter
{
    private long maxPeriod;
    private static long resetBefore = 0;

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException
    {
        try
        {
            boolean invalidated = false;

            HttpServletRequest request = (HttpServletRequest) req;
            HttpSession session = request.getSession( false );
            if ( session != null )
            {
                if(resetBefore > 0 && session.getCreationTime() < resetBefore)
                {
                    invalidated = true;
                    session.invalidate();
                }

                long activated = (long) session.getAttribute("activation-time");
                if (!invalidated && System.currentTimeMillis() > (activated + maxPeriod))
                {
                    session.invalidate();
                }

//                if (session.getAttribute("activation-time") == null)
//                {
//                    session.invalidate();
//                }
//                else
//                {
//                    long activated = (long) session.getAttribute("activation-time");
//                    if (System.currentTimeMillis() > (activated + maxPeriod)
//                        || activated < resetBefore)
//                    {
//                        session.invalidate();
//                    }
//                }
            }
        }
        catch(Exception e)
        {
            //right now don't log
            //e.printStackTrace();
        }
        chain.doFilter(req, res);
    }

    public void init(FilterConfig config) throws ServletException
    {
        //Get init parameter
        if ( config.getInitParameter("max-period") == null )
        {
            throw new IllegalStateException( "max-period must be provided" );
        }
        maxPeriod = new Long( config.getInitParameter("max-period") );
        //resetBefore = System.currentTimeMillis();
    }

    public void destroy()
    {
        //add code to release any resource
    }

    public static void ResetSessions()
    {
        resetBefore = System.currentTimeMillis();
    }
}