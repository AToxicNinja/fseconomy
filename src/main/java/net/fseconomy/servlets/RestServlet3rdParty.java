package net.fseconomy.servlets;

import net.fseconomy.beans.GoodsBean;
import net.fseconomy.data.Aircraft;
import net.fseconomy.data.Data;
import net.fseconomy.data.Goods;
import net.fseconomy.data.Stats;
import net.fseconomy.dto.AircraftRepair;
import net.fseconomy.dto.LatLonCount;
import net.fseconomy.dto.LatLonPair;
import net.fseconomy.dto.PilotStatus;
import net.fseconomy.services.ServiceData;
import net.fseconomy.util.Formatters;

import javax.annotation.security.PermitAll;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

import static net.fseconomy.services.common.*;

@Path("/api/tp/v1/")
@Produces({ "application/json;charset=UTF-8" })
public class RestServlet3rdParty
{
    @PermitAll
    @GET
    @Path("/aircraft/{key}")
    public Response getAircraft(@PathParam("key") final String key)
    {
        return ServiceData.getAircraftData(key);
    }

    @PermitAll
    @POST
    @Consumes("application/json")
    @Path("/aircraftrepair/{key}")
    public Response payAircraftRepair(@PathParam("key") final String key, AircraftRepair[] repairs)
    {
        return ServiceData.payAircraftRepair(key, repairs);
    }
}